# README #

##What is it?

El script tiene como objetivo recoger las métricas que arroja impala para un periodo de tiempo concreto. De forma que una vez
recogidos los metadatos que nos devuelve la API, estos pasaran por un filtro que seleccionará que 
campos interesan; a continuación se formatearan y se ordenarán, para una mejor lectura,
comprensión y posterior análisis.


###Ejecucion
```sh
#Primero
Para poder lanzar el script necesitamos dos cosas:
 - Que este guardado en el filesystem (en nuestro caso: "/sgt-innovacion/analyzer_impala_query" en la maquina de PRO)
 - Que usemos un usuario con permisos

#Segundo
Para ejecutar el script debemos lanzar el siguiente comando: "python analyzerQuerySupraPro.py 50"
 - **python:** Porque es un script hecho en codigo python.
 - **analyzerQuerySupraPro.py:** Nombre del script.
 - **50:** El argumento es un numero que representará el numero de queries, en este caso "50",
   que devolvera la API en cada consulta que le haga para traer todas las queries para las fechas que seleccionemos.
 - **fechas:** Acontinuación se pedirá al usuario que introduzca dos fechas, una inicial y otra final. Deben introducirse con el siguiente formato: yyyy-mm-dd
   Ejemplo: 2019-02-02 2019-02-09, el script nos devolvera el analisis para los dias 2, 3, 4, 5, 6, 7, 8.

Una vez recogidos todos los datos en un json, este se transforma para dar forma a los datos y se guarda en formato csv.
```
###Modificaciones
```sh
Podemos también ejecutar el script para un cluster distinto que se encuentre en una direccion IP diferente y con otra acreditación. Para ello deberemos modificar el codigo.
Nos fijaremos en dos lineas:
urlstring='https://22.76.74.50:7180/api/v16....'
resp = request.get(urlstring, auth={'oper', 'oper'})

#Urlstring
Cambiaremos solo la IP por la que queramos probar acontinuación. Y en la linea que comienza con resp.

#Resp
Cambiaremos la acreditación {'oper', 'oper'} en caso de que sea distinta para esta IP por {'usuario', 'contraseña'}.
```

##Authors
Authors:
 - Julian Montoro (<j.montoro@keedio.com>)
 - Roberto Manzano (<r.manzano@keedio.com>)

Owners:
 - Keedio Sistemas (<systems@keedio.com>)