#!/bin/bash 

echo "Empezando"

DATA_FILE="brute_data.json"
DATA_FILE_CSV="brute_data_filter.csv"
DATA_FILE_CSV_FORMATED="brute_data_formated.json"
# Format: YYYY-MM-DDTHH:mm:ss.mssZ"
FROM="2019-02-01T00:00:00.000Z"
TO="now"
DATA=""
OFFSET=0
LIMIT=$1

rm $DATA_FILE
touch $DATA_FILE
while true; do
        echo "Consultando desde: $FROM  hasta $TO"
	echo "hola<<<:$time_limit"
        OFFSET=0
        while true; do
                echo "++ Desde: $FROM hasta $TO OFF=$OFFSET LIM=$LIMIT"
                RESULT="$(curl -s -u oper:oper http://192.168.4.112:7180/api/v16/clusters/cluster/services/impala/impalaQueries?from=$FROM\&to=$TO\&limit=$LIMIT\&offset=$OFFSET\&limit=1000)"
                QUERIES="$(echo $RESULT | jq .queries)"
                if [ "$QUERIES" == "null" ] | [ "$QUERIES" == "[]" ]; then
                        break
                else
                        echo "$QUERIES" >> $DATA_FILE
                        OFFSET=$(($OFFSET + $LIMIT))
                fi
        done


        if [ -z $FROM ]; then
                echo "pepe"
		break
        fi

        if [ -z $time_limit ]; then
                echo "solar"
		break
        else
                ABS_TO=$time_limit
		echo "holayadios"
        fi

done

echo "Se han encontrado $(grep -o 'queryId' $DATA_FILE | wc -l) queries"
echo "Se han encontrado $(grep -o 'query_status": "OK"' $DATA_FILE | wc -l) queriesOK"
echo "Se han encontrado $(grep -c 'Exception' $DATA_FILE) queriesException"
echo "Se han encontrado $(grep -o 'query_status": "Canceled"' $DATA_FILE | wc -l) queriesCanceled"

rm $DATA_FILE_CSV
jq '.[] | [.user, .coordinator.hostId, .queryId, .queryType, .attributes.ddl_type, .attributes.memory_per_node_peak_node, .attributes.memory_per_node_peak, .attributes.query_status, .durationMillis / 1000 ] | @csv' $DATA_FILE > $DATA_FILE_CSV
rm $DATA_FILE_CSV_FORMATED

echo "UsuarioId,hostId,queryId,queryType,ddlType,memoryNode,queryStatus,durationMillis" >> $DATA_FILE_CSV_FORMATED
sed -e 's/\\//g' -e 's/"//g' $DATA_FILE_CSV >> $DATA_FILE_CSV_FORMATED
echo "Terminado"
