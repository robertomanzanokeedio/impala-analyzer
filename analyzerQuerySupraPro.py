#!/bin/python

import sys,os
import requests
import json
import copy
import csv
import collections
import datetime

print("Empezando")
from datetime import timedelta, date, datetime
#Introducir fecha inicial
while True:
    fecha_Ini= raw_input('\n Ingrese fecha Inicial "aaaa-mm-dd"...: ')
    try:
        fechaIni = datetime.strptime(fecha_Ini, "%Y-%m-%d")
    except ValueError:
        print("\n No ha ingresado una fecha correcta...")
    else:
        fecha_inicial = fechaIni - timedelta(days=1)
	fecha_ingreso_Ini = str(fecha_inicial)
        fecha_separator=[]
        fecha_separator = fecha_ingreso_Ini.split(' ')
        fecha_ingreso_from = fecha_separator[0]
        print fecha_ingreso_from
        break
#Introducir fecha final
while True:
    fecha_Final_str = raw_input('\n Ingrese fecha final "aaaa-mm-dd"...: ')
    try:
        fechaFinal = datetime.strptime(fecha_Final_str, "%Y-%m-%d")
    except ValueError:
        print("\n No ha ingresado una fecha correcta...")
    else:
        break
diferencia = fechaFinal - fecha_inicial
a,m,d = [int(v) for v in fecha_ingreso_from.split("-")]
fecha = date(a, m, d)
my_list_date=[]
my_prueba=[]
range_days = diferencia.days + 1
for i in range(1,range_days):
  newdate = fecha + timedelta(days=i)
  my_prueba.append(str(newdate))
  my_list_date.append(newdate.strftime("%y-%m-%d")) 

LIMIT = sys.argv[1]
OFFSET=0
j=0
limit_ini=0
limit_fini=1
my_list = []
time_limit = 0
my_list_filtered = []
responseModified = dict()
def union2(dict1, dict2):
    return dict(list(dict1.items()) + list(dict2.items()))

while True:
      j=j+1
      FROM=''
      if(j==diferencia.days):
        salir=0
        break
      if(j<diferencia.days):
        FROM=str(my_prueba[limit_ini])
	TO=str(my_prueba[limit_fini])
      while True:
            limit_ini=limit_ini+1
            limit_fini=limit_fini+1
            OFFSET=0
            while True:
                  print("Consultando desde: "+FROM+ "hasta"+TO)
                  ri = str(OFFSET)
                  urlstring ='http://192.168.4.112:7180/api/v16/clusters/cluster/services/impala/impalaQueries?from='+FROM+'&to='+TO+'&limit='+LIMIT+'&offset='+ri+'&limit=1000'
                  resp = requests.get(urlstring, auth=('oper', 'oper'))
                  print resp.url
                  response = json.loads(resp.content)
                  if(response == None or response['queries'] == []):
                    break
                  else:
                      my_list.append(response["queries"])
                      OFFSET = int(OFFSET) + int(LIMIT)

            if(FROM == None):
              break
            if(time_limit==0):
              break
            else:
                ABS_TO=time_limit
my_list_modified = [num for elem in my_list for num in elem]
response['queries'] = my_list_modified
with open('querifINAL.json', 'w') as outfile:
   json.dump(response, outfile) 
with open("querifINAL.json") as file:
   data = json.load(file)

headers = ["UsuarioId",
            "queryId",
            "queryType",
            "ddlType",
            "memoryNode",
            "queryStatus",
            "durationMillis",
            "queryState",
            "startTime",
            "endTime",
            "rowsProduced",
            "hdfs_bytes_written",
            "stats_missing",
            "pool",
            "session_type",
            "stats_corrupt",
            "estimated_per_node_peak_memory",
            "rows_inserted",
            "admission_result",
            "planning_wait_time",
            "admission_wait",
            "memory_per_node_peak",
            "coordinator"]

def flatDict(arrayIn):
    allQueries = []
    for item in arrayIn:
        dictInterno = {}
        for key, value in item.items():
            if type(value) is dict:
                for subKey, subValue in value.items():
                    dictInterno[subKey] = subValue
            else:
                dictInterno[key] = value
        dictInterno = dict(collections.OrderedDict(sorted(dictInterno.items())))
        allQueries.append(dictInterno)
    return allQueries

def normalizeDict(arrayIn):
    allQueries = []
    for item in arrayIn:

        for fixedKey in headers:
            if fixedKey not in item:
                item[fixedKey] = "null"

        dictInterno = item.copy()

        for key, value in item.items():
            if key not in headers:
                del dictInterno[key]

        #print dictInterno
        dictInterno = dict(collections.OrderedDict(sorted(dictInterno.items())))
        #print len(dictInterno)
        allQueries.append(dictInterno)
    return allQueries

#====================================================================================================

#main

#read JSON
with open("querifINAL.json") as file:
    data = json.load(file)


"""for item in dicc:
    print item, len(item)
    print

print len(headers)
"""
#Write CSV
with open("data.csv", "w") as file:
    csv_file = csv.writer(file)

    count = 1

    #Create dict
    dictToWrite = normalizeDict(flatDict(data['queries']))
    #Write header
    csv_file.writerow(dictToWrite[0].keys())

    for item in dictToWrite:
        """if count == 1:
            csv_file.writerow(dictFinal.keys())
            count = 0"""
        csv_file.writerow(item.values())
print( "Terminado")
