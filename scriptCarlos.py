#!/bin/python


import sys, os
import requests
import csv
import json

print("Empezando")

DATA_FILE = "brute24444_data.json"
# Format: YYYY-MM-DDTHH:mm:ss.mssZ"
FROM = "2019-02-01T00:00:00.000Z"
TO = "now"
DATA = ""
LIMIT = sys.argv[1]
OFFSET = 0

while True:

    print("Consultando desde: " + FROM + "hasta" + TO)

    ri = str(OFFSET)

    urlstring = 'http://192.168.4.112:7180/api/v16/clusters/cluster/services/impala/impalaQueries?from=' + FROM + '&to=' + TO + '&limit=' + LIMIT + '&offset=' + ri + '&limit=1000'

    resp = requests.get(urlstring, auth=('oper', 'oper'))

    print (resp.url)

    response = json.loads(resp.content)

    print(response)

    if (response == None or response['queries'] == []):

        break

    else:

        with open('test23.json', 'w') as f:

            json.dump(response, f)

            OFFSET = int(OFFSET) + int(LIMIT)

print("Terminado")

print(response)
#transformamos json a csv
infile = open('test23.json', 'r')
outfile = open('testRober.csv', 'w')
data = json.load(infile)
infile.close()
outfile = csv.writer(outfile)
for row in data['queries']:
    outfile.writerow(row.values())
outfile.close()

